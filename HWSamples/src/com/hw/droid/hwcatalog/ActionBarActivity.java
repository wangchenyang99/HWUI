package com.hw.droid.hwcatalog;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;

import hwdroid.app.HWActivity;
import hwdroid.widget.ActionBar.ActionBarView.OnLeftWidgetItemClick;
import hwdroid.widget.ActionBar.ActionBarView.OnOptionMenuClick;
import hwdroid.widget.ActionBar.ActionBarView.OnRightWidgetItemClick;
import hwdroid.widget.ActionBar.ActionBarView.OnCreateOptionMenu;

public class ActionBarActivity extends HWActivity {
	
    private boolean mOptionItemDisabledStatus = true;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContentView(R.layout.actionbar_layout); 
        showBackKey(true);
        setTitle2("1234567865443567543567578");
        setSubTitle2("subtitle.111122233445678788900");
        
        Button btn1 = (Button)this.findViewById(R.id.btn1);
        btn1.setText("show back key");
        
        btn1.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.showBackKey(true);
			}});
        
        Button btn2 = (Button)this.findViewById(R.id.btn2);
        btn2.setText("remove back key");        
        btn2.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.showBackKey(false);
			}});
        
        Button btn3 = (Button)this.findViewById(R.id.btn3);
        btn3.setText("add custom left view"); 
        btn3.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
                Button btn = new Button(ActionBarActivity.this);
                btn.setText("Button");
                
                ActionBarActivity.this.setLeftWidgetView(btn);
				ActionBarActivity.this.setLeftWidgetClickListener(new OnLeftWidgetItemClick() {

					@Override
					public void onLeftWidgetItemClick() {
						Toast.makeText(ActionBarActivity.this, "click left widget view!!", Toast.LENGTH_SHORT).show();
					}});
				}});
        
        Button btn4 = (Button)this.findViewById(R.id.btn4);
        btn4.setText("remove custom left view");  
        btn4.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.removeLeftWidgetView();
			}});
        
        
        Button btn5 = (Button)this.findViewById(R.id.btn5);
        btn5.setText("add custom right view"); 
        btn5.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
//				ImageView view = new ImageView(ActionBarActivity.this);
//				view.setImageResource(R.drawable.hw_footerbar_menu_item_more_icon);
//				view.setClickable(false);
			    
			    Button btn = new Button(ActionBarActivity.this);
			    btn.setText("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
			    
				ActionBarActivity.this.setRightWidgetView(btn);
				ActionBarActivity.this.setRightWidgetClickListener(new OnRightWidgetItemClick() {

					@Override
					public void onRightWidgetItemClick() {
						Toast.makeText(ActionBarActivity.this, "click right widget view!!", Toast.LENGTH_SHORT).show();
					}});
	
				}});
        
        Button btn6 = (Button)this.findViewById(R.id.btn6);
        btn6.setText("remove custom right view key"); 
        btn6.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.removeRightWidgetView();
			}});
        
        Button btn7 = (Button)this.findViewById(R.id.btn7);
        btn7.setText("set actionbar custom!"); 
        btn7.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.setActionBarBackgroudResource(R.drawable.aui2_5);
			}});  
        
        Button btn8 = (Button)this.findViewById(R.id.btn8);
        btn8.setText("set checkbox to rigth widget!"); 
        btn8.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				CheckBox cb = new CheckBox(ActionBarActivity.this);
				ActionBarActivity.this.setRightWidgetView(cb);	
				
				cb.setOnCheckedChangeListener(new OnCheckedChangeListener(){

					@Override
					public void onCheckedChanged(CompoundButton arg0,
							boolean arg1) {
						if(arg1) {
							ActionBarActivity.this.setTitle2("selected all!");
						} else {
							ActionBarActivity.this.setTitle2("not selected all!");
						}
						Toast.makeText(ActionBarActivity.this, "selected check box!", Toast.LENGTH_SHORT).show();
						
					}});
			}});  
        
        Button btn9 = (Button)this.findViewById(R.id.btn9);
        btn9.setText("option menu"); 
        btn9.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.setOptionTitle2("option title");
								
				CharSequence[] optionItems = {"option1", "option2", "option2"};
				
				ActionBarActivity.this.setOptionItems(optionItems, new OnOptionMenuClick() {

					@Override
					public void onOptionMenuClick(int pos) {
						Toast.makeText(ActionBarActivity.this, "option item = " + pos, Toast.LENGTH_SHORT).show();
					}});
			}});

        Button btn10 = (Button)this.findViewById(R.id.btn10);
        btn10.setText("setLeftWidgetItemEnabled"); 
        btn10.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.setLeftWidgetItemEnabled(false);
			}});

        Button btn11 = (Button)this.findViewById(R.id.btn11);
        btn11.setText("setRightWidgetItemEnabled"); 
        btn11.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				ActionBarActivity.this.setRightWidgetItemEnabled(false);
			}});
        
        Button btn12 = (Button)this.findViewById(R.id.btn12);
        btn12.setText("option menu(2): disabled item"); 
        btn12.setOnClickListener(new OnClickListener(){

            @Override
            public void onClick(View arg0) {
                ActionBarActivity.this.setOptionTitle2("option title");              
                ActionBarActivity.this.getActionBarView().setOptionItems(new OnCreateOptionMenu(){

                    @Override
                    public CharSequence[] onCreateOptionMenu() {
                        CharSequence[] optionItems = {"option1(disabled?)", "option2(disabled?)", "option2(disabled?)"};
                        return optionItems;
                    }

                    @Override
                    public boolean[] onOptionMenuDisabled() {
                        mOptionItemDisabledStatus = !mOptionItemDisabledStatus;
                        
                        if(mOptionItemDisabledStatus == true) {
                            boolean[] disables = {true, false, true};
                            return disables;
                        }else {
                            boolean[] disables = {false, true, false};
                            return disables; 
                        }
                    }},
                    
                    new OnOptionMenuClick() {

                    @Override
                    public void onOptionMenuClick(int pos) {
                        Toast.makeText(ActionBarActivity.this, "option item = " + pos, Toast.LENGTH_SHORT).show();
                    }});
            }});
    }

}

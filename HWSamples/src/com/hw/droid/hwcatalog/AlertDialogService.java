package com.hw.droid.hwcatalog;

import hwdroid.dialog.AlertDialog;
import hwdroid.dialog.Dialog;
import hwdroid.dialog.DialogInterface;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.view.WindowManager;

public class AlertDialogService extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}	
	
	@SuppressWarnings("deprecation")
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		
		CharSequence[] items = new CharSequence[]{"item1", "item2", "item3"};
        Dialog alertDialog = new AlertDialog.Builder(this). 
                setTitle("title"). 
                setCancelable(false).
                setItems(items, new DialogInterface.OnClickListener(){

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						
					}}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() { 
	                     
	                    @Override 
	                    public void onClick(DialogInterface dialog, int which) { 
	                    } 
	                }).create(); 
        
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT); 
        alertDialog.show();  
	}
}

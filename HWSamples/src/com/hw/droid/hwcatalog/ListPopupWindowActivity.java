package com.hw.droid.hwcatalog;

import hwdroid.app.HWActivity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListPopupWindow;


public class ListPopupWindowActivity extends HWActivity 
	implements OnItemClickListener{
	
	EditText productName;
	ListPopupWindow listPopupWindow;
	String[] products={"Camera", "Laptop", "Watch","Smartphone",
    	"Television", "a", "b", "c", "d", "e", "xx", "yy", "dd"};


	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setActivityContentView(R.layout.activity_list_popup_window);
		this.showBackKey(true);
		
        productName = (EditText) findViewById(R.id.popupwindow_test_edit);
        listPopupWindow = new ListPopupWindow(ListPopupWindowActivity.this);
        listPopupWindow.setAdapter(new ArrayAdapter(ListPopupWindowActivity.this, R.layout.list_item, products));
        listPopupWindow.setAnchorView(productName);
        listPopupWindow.setModal(true);
        listPopupWindow.setOnItemClickListener(ListPopupWindowActivity.this);
        
        productName.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                listPopupWindow.show();
            }
        });
	}

	@Override
    public void onItemClick(AdapterView<?> parent, View view,
        int position, long id) {
        productName.setText(products[position]);
        listPopupWindow.dismiss();
    }
}

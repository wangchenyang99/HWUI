
package com.hw.droid.hwcatalog.indicator;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.hw.droid.hwcatalog.R;

import hwdroid.app.BaseFragmentActivity;
import hwdroid.widget.ActionBar.ActionBarView;
import hwdroid.widget.indicator.HWTabPageSimpleIndicator;

public class SimpleTabIndicatorActivity extends BaseFragmentActivity {
    private static final String[] CONTENT = new String[] {
            "A", "B", "C", "D"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActivityContentView(R.layout.activity_simple_indicator);

        getFooterBarImpl().setVisibility(View.GONE);

        FragmentPagerAdapter adapter = new SampleFragmentPagerAdapter(
                this.getSupportFragmentManager());

        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

//        HWTabPageSimpleIndicator indicator = (HWTabPageSimpleIndicator) findViewById(R.id.tabindicator);
        HWTabPageSimpleIndicator indicator = new HWTabPageSimpleIndicator(this);
        
        ActionBarView actionbarView = (ActionBarView) findViewById(R.id.actionbarview);
        
//      indicator = (HWTabPageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(pager);
        actionbarView.removeAllViews();
        actionbarView.addView(indicator, 
                new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        
        indicator.setCurrentItem(2);
    }

    class SampleFragmentPagerAdapter extends FragmentPagerAdapter {
        public SampleFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return TestFragment.newInstance(CONTENT[position % CONTENT.length]);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return CONTENT[position % CONTENT.length].toUpperCase();
        }

        @Override
        public int getCount() {
            return CONTENT.length;
        }
    }
}
